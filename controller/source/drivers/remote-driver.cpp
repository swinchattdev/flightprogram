#include <cerrno>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <iomanip>
#include <iostream>
#include <memory>
#include <mutex>
#include <sstream>
#include <string>
#include <thread>
#include <vector>

#include <dirent.h>
#include <sys/ioctl.h>
#include <sys/wait.h>
#include <unistd.h>

#include <fp/common.hpp>
#include <fp/drivers/common.hpp>
#include <fp/drivers/remote-driver.hpp>
#include <fp/util/fork-exec.hpp>
#include <fp/util/string-util.hpp>

#ifndef DRIVERS_PATH
# define DRIVERS_PATH "/opt/flightprogram/drivers"
#endif

using fp::util::redirection;
using fp::util::redirection_t;
using fp::util::fork_exec;
using fp::util::tokenise;
using fp::util::format_strerror_message;

namespace fp { namespace drivers {
std::vector<std::shared_ptr<remote_driver>> remote_driver::_drivers;

void remote_driver::update_driver_list(const std::string& path)
{
    DIR* dir = opendir(path.c_str());
    if (!dir) {
        std::clog << format_strerror_message(std::string("opendir ") + path) << std::endl;
        return;
    }

    while (true) {
        auto entry = readdir(dir);        
        if (entry == nullptr) {
            break;
        }

        std::ostringstream ss;
        ss << path << '/' << entry->d_name;
        if (access(ss.str().c_str(), X_OK) == 0) {
            _drivers.emplace_back(new remote_driver(ss.str()));
        }
    }

    closedir(dir);
}

void remote_driver::list(std::vector<std::shared_ptr<remote_driver>>& drivers)
{
    list(DRIVERS_PATH, drivers);
}

void remote_driver::list(const std::string& path, std::vector<std::shared_ptr<remote_driver>>& drivers)
{
    if (!_drivers.size()) {
        update_driver_list(path);
    }

    for (auto driver : _drivers) {
        drivers.push_back(driver);
    }
}

remote_driver::remote_driver(const std::string& filename) : _filename(filename)
{}

static void parse_child_info(int fd, std::string& name, std::vector<device_info>& info)
{
    char buffer[4096];
    std::string line;
    while (true) {
        auto count = read(fd, buffer, sizeof(buffer));
        if (count < 0) {
            perror("read");
            break;
        }

        for (auto i = 0l; i < count; i++) {
            if (buffer[i] == '\n') {
                if (line.size() == 0) {
                    continue;
                }

                std::vector<std::string> tokens;
                tokenise(line, " : ", tokens);
                auto unexpected = false;
                if (tokens.size() != 2) {
                    unexpected = true;
                } else if (tokens[0] == "Name") {
                    name = tokens[1];
                } else {
                    int count;
                    std::stringstream ss(tokens[1]);
                    ss >> count;
                    if (tokens[0] == "accelerometer") {
                        info.emplace_back(device_type::accelerometer, count);
                    } else if (tokens[0] == "barometer") {
                        info.emplace_back(device_type::barometer, count);
                    } else if (tokens[0] == "gyroscope") {
                        info.emplace_back(device_type::gyroscope, count);
                    } else if (tokens[0] == "magnetometer") {
                        info.emplace_back(device_type::magnetometer, count);
                    } else if (tokens[0] == "servo_esc") {
                        info.emplace_back(device_type::servo_esc, count);
                    } else if (tokens[0] == "thermometer") {
                        info.emplace_back(device_type::thermometer, count);
                    } else {
                        unexpected = true;
                    }
                }

                if (unexpected) {
                    std::cerr << "Child process gave unexpected output \"" << line << "\"\n";
                }

                line.erase();
            } else {
                line.push_back(buffer[i]);
            }
        }
                
        if (count < sizeof(buffer)) {
            break;
        }
    }
}

void remote_driver::ensure_info()
{
    if (_name.size() || _device_info.size()) {
        return;
    }

    std::vector<std::string> args {"-info"};
    int fd[2];
    auto pid = fork_exec(_filename, args, fd, redirection::stdout);
    int status;
    if (waitpid(pid, &status, 0) < 0) {
        perror("waitpid");
        return;
    }

    if (WEXITSTATUS(status)) {
        std::cerr << "Child process returned non-zero status code" << std::endl;
        return;
    }

    parse_child_info(fd[0], _name, _device_info);
}

const std::string& remote_driver::name()
{
    ensure_info();
    return _name;
}

const std::vector<device_info>& remote_driver::get_device_info()
{
    ensure_info();
    return _device_info;
}

bool remote_driver::running() const
{
    return _pid > 0;
}

struct process_watcher_thread_stopper;

struct process_watcher {
    friend struct process_watcher_thread_stopper;

    static void start(int pid, std::atomic<int>& status)
    {
        std::lock_guard<std::mutex> guard(_lock);
        if (_thread == nullptr) {
            _thread = new std::thread(&run);
        }

        _watchers.push_back(new process_watcher(pid, status));
    }

private:
    static std::vector<process_watcher*> _watchers;
    static std::mutex _lock;
    static std::atomic<bool> _stop;
    static std::thread* _thread;

    static void run()
    {
        int status;
        while (!_stop.load()) {
            // Wait for any child process to exit.
            auto pid = waitpid(-1, &status, 0);

            // While under lock, find the process, update its status and delete the watcher.
            std::lock_guard<std::mutex> guard(_lock);
            for (auto it = _watchers.begin(); it < _watchers.end(); it++) {
                if ((*it)->_pid == pid) {
                    (*it)->_status = WEXITSTATUS(status);
                    _watchers.erase(it);
                    delete (*it);
                    break;
                }
            }

            // We don't verify that the process with PID 'pid' was removed because this child process could belong to
            // a different part of the application.
        }
    }

    int _pid;
    std::atomic<int>& _status;

    process_watcher(int pid, std::atomic<int>& status) : _pid(pid), _status(status)
    {}
};

std::vector<process_watcher*> process_watcher::_watchers;
std::mutex process_watcher::_lock;
std::atomic<bool> process_watcher::_stop;
std::thread* process_watcher::_thread;

// Signals the process_watcher thread to stop, joins it if it's still running, then frees its resources.
static struct process_watcher_thread_stopper {
    ~process_watcher_thread_stopper()
    {
        if (process_watcher::_thread) {
            process_watcher::_stop = true;
            process_watcher::_thread->join();
            for (auto w : process_watcher::_watchers) {
                delete w;
            }
        }
    }
} process_watcher_thread_stopper_instance;

int remote_driver::spawn()
{
    std::vector<std::string> args;
    int fd[2];
    _pid = fork_exec(_filename, args, fd, static_cast<redirection_t>(redirection::stdout|redirection::stderr));
    if (_pid < 0) {
        return -1;
    }

    process_watcher::start(_pid, _status);
    return _pid;
}

int remote_driver::terminate()
{
    if (_pid < 0) {
        return _status.load();
    }

    if (kill(_pid, SIGTERM) < 0) {
        perror("kill");
        return -1;
    }

    int status;
    if (waitpid(_pid, &status, 0) < 0) {
        perror("waitpid");
        return -1;
    }

    _pid = -1;
    _status = WEXITSTATUS(status);
    return _status;
}

int remote_driver::status() const
{
    return _status.load();
}
}} // fp::drivers
