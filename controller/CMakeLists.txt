add_executable(flightprogramd)

target_include_directories(
    flightprogramd
        PRIVATE
            ../common/include
            ./include
)

target_sources(
    flightprogramd
        PRIVATE
            ./source/main.cpp
            ./source/drivers/remote-driver.cpp
)

target_link_libraries(flightprogramd fpcommon pthread)
