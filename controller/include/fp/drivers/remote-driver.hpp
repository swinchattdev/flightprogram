/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <atomic>
#include <memory>
#include <string>
#include <vector>

#include <fp/common.hpp>
#include <fp/drivers/common.hpp>

namespace fp { namespace drivers {
struct remote_driver {
    /// Add available drivers in the default search directory to 'drivers'.
    static void list(std::vector<std::shared_ptr<remote_driver>>& drivers);

    /// Add available drivers in the specified search directory to 'drivers'.
    static void list(const std::string& path, std::vector<std::shared_ptr<remote_driver>>& drivers);

    /// Construct a remote_driver instance from a filename. Note: This does not spawn a subprocess.
    remote_driver(const std::string& filename);

    /// Gets the driver's reported device name. May trigger an invocation of the driver process.
    const std::string& name();

    /// Gets the driver's reported device info. May trigger an invocation of the driver process.
    const std::vector<device_info>& get_device_info();

    /// Indicates whether the driver is running.
    bool running() const;

    /// Gets the current PID or -1 if the process is not running.
    int pid() const;

    /// Spawns the driver as a subprocess.
    /// \returns The PID of the subprocess if successful, otherwise -1.
    int spawn();

    /// Sends the TERM signal to the subprocess and waits for it to terminate.
    /// \returns The subprocess' exit status if successful, otherwise -1.
    /// \note It is a no-op if the process is not running, in which case the previous status or 0 will be returned.
    int terminate();

    /// Gets the exit status that was returned the last time the process exited.
    int status() const;
private:
    static std::vector<std::shared_ptr<remote_driver>> _drivers;

    static void update_driver_list(const std::string& path);

    std::string _filename;
    std::string _name;
    std::vector<device_info> _device_info;
    int _pid = -1;
    std::atomic<int> _status;

    void ensure_info();
};
}} // fp::drivers
