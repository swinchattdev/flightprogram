/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <iomanip>
#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include <fp/common.hpp>
#include <fp/drivers/i2c-bus.hpp>
#include <fp/drivers/standalone-driver.hpp>

namespace fp { namespace drivers {
struct mpu9250 : standalone_driver {
    const char* name() override
    {
        return "MPU-9250 9-axis IMU";
    }

    void add_options(std::vector<cmdline_option>& options) override
    {}

    void get_info(std::vector<device_info>& info) override
    {
        auto detected = detect();
        info.push_back(device_info(device_type::accelerometer, detected));
        info.push_back(device_info(device_type::gyroscope, detected));
        info.push_back(device_info(device_type::magnetometer, detected));
        info.push_back(device_info(device_type::thermometer, detected));
    }

    int init(const std::map<std::string, std::string>& args) override
    {
        return 0;
    }

    void update_sensor(packet* pkt) override
    {
        AccelGyroTempBuf buf1;
        if (read_accel_gyro_temp(buf1)) {
            return;
        }

        MagBuf buf2;
        if (read_mag(buf2)) {
            return;
        }

        for (auto i = 0u; i < pkt->count; i++) {
            auto& entry = pkt->entry[i];
            switch (entry.type) {
                case device_type::accelerometer:
                    entry.values[0] = buf1.get_float(0);
                    entry.values[1] = buf1.get_float(1);
                    entry.values[2] = buf1.get_float(2);
                    break;
                case device_type::gyroscope:
                    entry.values[0] = buf1.get_float(3);
                    entry.values[1] = buf1.get_float(4);
                    entry.values[2] = buf1.get_float(5);
                    break;
                case device_type::thermometer:
                    entry.values[0] = buf1.get_float(6);
                    break;
                case device_type::magnetometer:
                    entry.values[0] = buf2.get_float(0);
                    entry.values[1] = buf2.get_float(1); 
                    entry.values[2] = buf2.get_float(2);
                    break;
                default:
                    break;
            }
        }
    }

    void update_actuator(const packet* pkt) override
    {}
private:
    struct Reg {
        enum {
            ACCEL_XOUT_H = 0x3b,
            ACCEL_XOUT_L = 0x3c,
            ACCEL_YOUT_H = 0x3d,
            ACCEL_YOUT_L = 0x3e,
            ACCEL_ZOUT_H = 0x3f,
            ACCEL_ZOUT_L = 0x40,
            TEMP_OUT_H   = 0x41,
            TEMP_OUT_L   = 0x42,
            GYRO_XOUT_H  = 0x43,
            GYRO_XOUT_L  = 0x44,
            GYRO_YOUT_H  = 0x45,
            GYRO_YOUT_L  = 0x46,
            GYRO_ZOUT_H  = 0x47,
            GYRO_ZOUT_L  = 0x48,

            MAG_XOUT_L   = 0x03,
            MAG_XOUT_H   = 0x04,
            MAG_YOUT_L   = 0x05,
            MAG_YOUT_H   = 0x06,
            MAG_ZOUT_L   = 0x07,
            MAG_ZOUT_H   = 0x08,

            WHO_AM_I     = 0x75
        };
    };

    struct Val {
        enum {
            WHO_AM_I = 0x71
        };
    };

    struct AccelGyroTempBuf {
        uint8_t accel_x_h;
        uint8_t accel_x_l;
        uint8_t accel_y_h;
        uint8_t accel_y_l;
        uint8_t accel_z_h;
        uint8_t accel_z_l;
        uint8_t gyro_x_h;
        uint8_t gyro_x_l;
        uint8_t gyro_y_h;
        uint8_t gyro_y_l;
        uint8_t gyro_z_h;
        uint8_t gyro_z_l;
        uint8_t temp_h;
        uint8_t temp_l;

        float get_float(int idx) const
        {
            static_assert(sizeof(float) == 2));
            return reinterpret_cast<float*>(this)[idx];
        }
    } __attribute__((packed));

    struct MagBuf {
        uint8_t xl;
        uint8_t xh;
        uint8_t yl;
        uint8_t yh;
        uint8_t zl;
        uint8_t zh;

        float get_float(int idx) const
        {
            static_assert(sizeof(float) == 2));
            uint8_t bs[2];
            bs[1] = reinterpret_cast<float*>(this)[idx];
            bs[0] = reinterpret_cast<float*>(this)[idx + 1];
            return *reinterpret_cast<float*>(bs);
        }
    } __attribute__((packed));

    static constexpr uint16_t i2c_addr_main = 0b1101000;
    static constexpr uint16_t i2c_addr_aux = 0b1101001;

    int _bus_id;
    std::shared_ptr<i2c_device> _device;

    void log_read_error(int bus, int reg)
    {
        std::clog << "Warn: error reading from i2c-" << bus << ":" << std::hex << reg << "\n";
    }

    void log_detected(int bus, int addr)
    {
        std::clog << "Found " << name() << " at i2c-" << bus << ":" << std::hex << addr << "\n";
    }

    bool detect_device(std::shared_ptr<i2c_bus> bus, int bus_id, uint16_t addr, std::shared_ptr<i2c_device>& dev)
    {
        dev = bus->get_device(addr);
        auto whoami = dev->read(Reg::WHO_AM_I);
        if (whoami == Val::WHO_AM_I) {
            log_detected(bus_id, addr);
            return true;
        } else if (whoami < 0) {
            log_read_error(bus_id, Reg::WHO_AM_I);
        }

        return false;
    }

    // Look for the device across all i2c buses. Count the total number of detected devices across all buses and set
    // _device to the first. We look for the device at its main and auxiliary i2c addresses but only count a single
    // device when it is present at both.
    int detect()
    {
        std::shared_ptr<i2c_device> dev;
        auto detected = 0;
        for (int i = 0; i <= i2c_bus::get_max_bus_id(); i++) {
            auto bus = i2c_bus::get_instance(i);
            if (detect_device(bus, i, i2c_addr_main, dev) || detect_device(bus, i, i2c_addr_aux, dev)) {
                _device = dev;
                _bus_id = i;
                detected++;
            }
        }

        return detected;
    }

    int read_accel_gyro_temp(AccelGyroTempBuf& buf)
    {
        return _device->read(Reg::ACCEL_XOUT_H, &buf, sizeof(buf));
    }

    int read_mag(MagBuf& buf)
    {
        return _device->read(Reg::MAG_XOUT_L, &buf, sizeof(buf));
    }
};

standalone_driver& standalone_driver::get_instance()
{
    static mpu9250 instance;
    return instance;
}
}} // fp::drivers
