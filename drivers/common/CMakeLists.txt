add_library(fpdriver SHARED)

target_include_directories(
    fpdriver
        PUBLIC
            ../../common/include/
            ./include/
)

target_sources(
    fpdriver
        PUBLIC
            ./source/program.cpp

            ./source/i2c-bus/linux-i2c-bus.cpp

            ./source/transports/transport.cpp
            ./source/transports/stdio-transport.cpp
)

target_link_libraries(fpdriver fpcommon)
