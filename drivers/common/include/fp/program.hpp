/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <iosfwd>
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <fp/common.hpp>
#include <fp/program.hpp>
#include <fp/drivers/standalone-driver.hpp>
#include <fp/util/cmdline.hpp>
#include <fp/transports/transport.hpp>

namespace fp {
/// Initialises and runs the driver & transport.
struct program final {
    /// Construct from program arguments.
    program(int argc, char** argv);

    /// Run the program.
    /// \returns 0 if successful, otherwise a value from 1 to 255.
    int run();

private:
    enum class handle_argument_result {
        push,
        cont
    };

    struct transport_event_handler : transports::transport::event_handler {
        transport_event_handler(drivers::standalone_driver& driver) : _driver(driver)
        {}

        void on_sensor_update(drivers::packet* pkt) override
        {
            _driver.update_sensor(pkt);
        }

        void on_actuator_update(const drivers::packet* pkt) override
        {
            _driver.update_actuator(pkt);
        }
    private:
        drivers::standalone_driver& _driver;
    };

    std::string _program_name;
    int _argc;
    char** _argv;
    drivers::standalone_driver& _driver;
    std::map<std::string, std::string> _vm;
    std::vector<util::cmdline_option> _options;
    std::vector<std::string> _positionals;
    std::vector<std::shared_ptr<transports::transport>> _transports;
    std::shared_ptr<transports::transport> _transport;
    std::shared_ptr<transport_event_handler> _event_handler;

    int first_time_init();

    void add_options();

    void show_usage(std::ostream& stream);

    void die_usage(const std::string& error);

    int process_command_line();

    void show_info();

    int check_args();

    int process_args();
};
} // fp