/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <map>
#include <memory>
#include <string>
#include <vector>

#include <fp/drivers/common.hpp>
#include <fp/util/cmdline.hpp>

namespace fp { namespace transports {
struct transport;

struct transport_registrar {
protected:
    void register_transport(const char* name, std::shared_ptr<transport> instance);
};

/// Interface implemented by driver-side transports, which handle communication between standalone drivers and the
/// control program.
struct transport : fp::util::has_cmdline_options {
    friend struct transport_registrar;

    /// Transport event handler.
    struct event_handler {
        virtual ~event_handler() {}

        virtual void on_sensor_update(drivers::packet* pkt) {}

        virtual void on_actuator_update(const drivers::packet* pkt) {}
    };

    /// Gets the named transport.
    static std::shared_ptr<transport> get(const char* name);

    /// Adds registered transports to 'transports'.
    static void list_available_transports(std::vector<std::shared_ptr<transport>>& transports);

    /// Gets the event handler. Never returns nullptr; if none was set a pointer to a default instance is returned.
    std::shared_ptr<event_handler> get_event_handler();

    virtual ~transport() {}

    /// Sets the event handler. A nullptr resets to a default instance.
    void set_event_handler(std::shared_ptr<event_handler> handler);

    /// Gets the transport's name.
    virtual const char* name() const = 0;

    /// Initialise with command-line variables.
    virtual int init(const std::map<std::string, std::string>& vm) override { return 0; }

    /// Runs the transport once.
    virtual void run_once() = 0;

    /// Runs the transport forever.
    virtual void run_forever() = 0;

private:
    static std::map<const char*, std::shared_ptr<transport>> _registrations;

    static void register_transport(const char* name, std::shared_ptr<transport> instance);

    std::shared_ptr<event_handler> _handler;
};
}} // fp::transports
