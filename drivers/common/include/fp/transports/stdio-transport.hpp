/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <fp/transports/transport.hpp>

namespace fp { namespace transports {
/// Provides a REPL-like environment that users can use to control drivers over the standard I/O streams.
struct stdio_transport : transport {
    /// \copydoc fp::util::has_cmdline_options::add_options
    void add_options(std::vector<fp::util::cmdline_option>& options) override;
    
    /// \copydoc fp::transport::name
    const char* name() const override;

    /// \copydoc fp::transport::run_once
    void run_once() override;

    /// \copydoc fp::transport::run_forever
    void run_forever() override;
private:
    bool _running;

    void help();

    void display();

    void quit();
};

}} // fp::transports
