/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <cstddef>
#include <cstdint>
#include <memory>

namespace fp { namespace drivers {
/// I2C device.
struct i2c_device {
    /// Reads a single byte from a register.
    /// \param[in] reg The register to read from.
    /// \returns The byte read if successful, otherwise -1.
    virtual int read(uint8_t reg) = 0;

    /// Reads several bytes from a register. It is also possible to read contigous registers this way.
    /// \param[in] reg The register to read from.
    /// \param[out] buffer The buffer to read into.
    /// \param[in,out] count When the function is called, contains the number of bytes to read. After the function 
    /// returns, contains the number of bytes actually read.
    /// \returns 0 if successful, otherwise -1.
    virtual int read(uint8_t reg, uint8_t* buffer, size_t& count) = 0;

    /// Write a single byte to a register.
    /// \param[in] reg The register to write to.
    /// \returns 0 if successful, otherwise -1.
    virtual int write(uint8_t reg, uint8_t byte) = 0;

    /// Writes several bytes to a register. It is also possible to write contiguous registers this way.
    /// \param[in] reg The register to write to.
    /// \param[out] buffer The buffer to write.
    /// \param[in,out] count When the function is called, contains the number of bytes to write. After the function 
    /// returns, contains the number of bytes actually written.
    /// \returns 0 if successful, otherwise -1.
    virtual int write(uint8_t reg, const uint8_t* buffer, size_t& count) = 0;
};

/// I2C bus.
/// \example
///     // Detect I2C device.
///     for (auto i = 0; i <= i2c_bus::get_max_bus_id(); i++) {
///         auto bus = i2c_bus::get_instance(i);
///         if (bus != nullptr) {
///             auto dev = bus->get_device(DEVICE_ADDRESS);
///             if (dev->read(WHOAMI_REGISTER) == WHOAMI_VALUE) {
///                 do_something_with(dev);
///                 break;
///             }
///         }
///     }
struct i2c_bus
{
    /// Gets the maximum value which is a valid bus ID.
    static int get_max_bus_id();

    /// Gets a pointer to the instance of the I2C bus with ID 'bus_id' if it exists.
    /// \param[in] bus_id The bus ID.
    /// \returns A pointer to an instance of the I2C bus, or nullptr if the bus does not exist.
    static std::shared_ptr<i2c_bus> get_instance(int bus);

    /// Gets an instance of an i2c device connected to the bus. Does not attempt to detect whether the device is
    /// actually connected.
    virtual std::shared_ptr<i2c_device> get_device(uint16_t address) = 0;
};
}}
