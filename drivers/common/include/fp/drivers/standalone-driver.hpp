/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <cstdint>
#include <map>
#include <string>
#include <vector>

#include <fp/common.hpp>
#include <fp/drivers/common.hpp>
#include <fp/util/cmdline.hpp>

namespace fp { namespace drivers {
using ::fp::util::cmdline_option;

/// Interface implemented by drivers. This is used in 'common/drivers/main.cpp'.
struct standalone_driver : fp::util::has_cmdline_options {
    /// Gets an instance of the driver. Should just construct or return an object.
    static standalone_driver& get_instance();

    /// Gets a friendly name for the device which is driven.
    virtual const char* name() = 0;

    /// Adds supported, detected devices.
    virtual void get_info(std::vector<device_info>& info) = 0;

    /// Called on a sensor driver to request updated sensor values.
    /// \param[out] pkt The packet to store the values into. The transport knows how many values to expect for
    /// each device type (e.g. x, y and z for accelerometer) and the memory is preallocated. All the driver has to do is
    /// fill the values[] array with the right number of values for each device type in entries[]. The number of entries
    /// will match the number of types that were reported in \see add_types.
    virtual void update_sensor(packet* pkt) = 0;

    /// Called on an actuator driver to update the signal.
    /// \param[in] pkt The packet the signal values are stored in. The transport knows how many values to provide for
    /// each device type.
    virtual void update_actuator(const packet* pkt) = 0;
};
}} // fp::drivers
