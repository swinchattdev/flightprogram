/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cstdlib>
#include <exception>
#include <iostream>
#include <memory>
#include <stdexcept>
#include <string>
#include <vector>

#include <fp/common.hpp>
#include <fp/program.hpp>
#include <fp/drivers/standalone-driver.hpp>
#include <fp/util/cmdline.hpp>
#include <fp/transports/transport.hpp>

using namespace fp::drivers;
using namespace fp::util;
using namespace fp::transports;
using event_handler = transport::event_handler;

namespace fp {
program::program(int argc, char** argv)
: _program_name(argv[0])
, _argc(argc)
, _argv(argv)
, _driver(standalone_driver::get_instance())
, _event_handler(std::make_shared<transport_event_handler>(_driver))
{}

int program::run()
{
    if (_argc) {
        auto error = first_time_init();
        if (error) {
            return error;
        }
    }

    _transport->set_event_handler(std::static_pointer_cast<event_handler>(_event_handler));
    _transport->run_forever();

    return 0;
}

int program::first_time_init()
{
    add_options();

    auto error = process_args();
    if (error) {
        return error;
    }

    error = _driver.init(_vm);
    if (error) {
        return error;
    }

    error = _transport->init(_vm);
    if (error) {
        return error;
    }

    _argc = 0;
    _argv = nullptr;
    _vm.clear();
    _options.clear();
    _positionals.clear();
    _transports.clear();
    return 0;
}

void program::add_options()
{
    // Add top-level options.
    _options.emplace_back("help", "Show this help and quit");
    _options.emplace_back("info", "Show device information and quit");

    // Add driver options.
    {
        std::vector<cmdline_option> tmp;
        _driver.add_options(tmp);
        if (tmp.size()) {
            for (auto& option : tmp) {
                option.section = "Driver options";
                _options.push_back(option);
            }
        }
    }

    // Add transport options.
    {
        transport::list_available_transports(_transports);
        if (_transports.size() == 0) {
            throw std::runtime_error("No transports were enabled during compilation, driver cannot run");
        }

        std::vector<cmdline_option> tmp;
        for (auto tp : _transports) {
            tmp.clear();
            tp->add_options(tmp);
            if (tmp.size()) {
                for (auto& option : tmp) {
                    option.section = std::string("Transport \"") + tp->name() + "\" options";
                    _options.push_back(option);
                }
            } else {
                _options.emplace_back(std::string("Transport \"") + tp->name() + "\" options", "", "");
            }
        }
    }
}

void program::show_usage(std::ostream& stream)
{
    stream << "Usage: " << _program_name << " [option(s)] <transport> [transport options]\n\n"
            << "Standalone driver for " << _driver.name() << ".\n\n"
            << "Positional arguments:\n"
            << "\ttransport\tName of transport to use (defaults to stdio; see below for available transports).\n"
    ;

    print_options(_options, stream, "\t", 0);
}

void program::die_usage(const std::string& error)
{
    show_usage(std::cerr);
    std::cerr << "\nError: " << error << ".\n";
    std::exit(1);
}

int program::process_command_line()
{
    auto result = parse_args(_argc, _argv, _options, _vm, _positionals);
    if (!result.ok()) {
        die_usage(result.error());
    }

    return 0;
}

void program::show_info()
{
    std::vector<device_info> info_array;
    _driver.get_info(info_array);
    if (info_array.size() == 0) {
        throw std::logic_error("Driver does not report any supported device types");
    }

    std::cout << "Name : " << _driver.name() << '\n';
    for (auto info : info_array) {
        std::cout << device_type_str(info.type) << " : " << info.count << '\n';
    }

    std::cout << std::flush;
}

int program::check_args()
{
    if (_vm.find(std::string("help")) != _vm.end()) {
        show_usage(std::cout);
        std::exit(0);
    }

    if (_vm.find(std::string("info")) != _vm.end()) {
        show_info();
        std::exit(0);
    }

    for (auto tp : _transports) {
        for (auto arg : _positionals) {
            if (arg == tp->name()) {
                _transport = tp;
                break;
            }
        }

        if (_transport.get() != nullptr) {
            break;
        }
    }

    if (_transport.get() == nullptr) {
        die_usage("No valid transport was specified");
        return 1;
    }

    return 0;
}

int program::process_args()
{
    auto error = process_command_line();
    if (error) {
        return error;
    }

    error = check_args();
    if (error) {
        return error;
    }

    return 0;
}
} // fp