/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <map>
#include <memory>
#include <string>
#include <vector>

#include <fp/transports/transport.hpp>

namespace fp { namespace transports {
std::map<const char*, std::shared_ptr<transport>> transport::_registrations;

void transport_registrar::register_transport(const char* name, std::shared_ptr<transport> instance)
{
    transport::register_transport(name, instance);
}

void transport::register_transport(const char* name, std::shared_ptr<transport> instance)
{
    _registrations.insert(std::make_pair(name, instance));
}

void transport::list_available_transports(std::vector<std::shared_ptr<transport>>& transports)
{
    for (auto pair : _registrations) {
        transports.push_back(pair.second);
    }
}

std::shared_ptr<transport> transport::get(const char* name)
{
    auto it = _registrations.find(name);
    if (it == _registrations.end()) {
        return std::shared_ptr<transport>(nullptr);
    }

    return it->second;
}

std::shared_ptr<transport::event_handler> transport::get_event_handler()
{
    return _handler;
}

void transport::set_event_handler(std::shared_ptr<event_handler> handler)
{
    static std::shared_ptr<transport::event_handler> default_event_handler(new transport::event_handler);
    if (handler.get() == nullptr) {
        _handler = default_event_handler;
    } else {
        _handler = handler;
    }
}
}} // fp::transports
