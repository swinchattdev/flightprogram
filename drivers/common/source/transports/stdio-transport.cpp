/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <array>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>

#include <fp/transports/transport.hpp>
#include <fp/transports/stdio-transport.hpp>

using namespace fp::drivers;
using namespace fp::util;

namespace fp { namespace transports {
struct stdio_transport_registrar : transport_registrar {
    stdio_transport_registrar()
    {
        std::shared_ptr<transport> instance(reinterpret_cast<transport*>(new stdio_transport()));
        this->register_transport(instance->name(), instance);
    }
private:
};

static stdio_transport_registrar stdio_transport_registrar_instance;

void stdio_transport::add_options(std::vector<cmdline_option>& options)
{
}

const char* stdio_transport::name() const
{
    return "stdio";
}

void stdio_transport::run_once()
{
    std::cout << "Command> " << std::flush;
    if (std::cin.eof()) {
        quit();
        return;
    }

    switch (std::cin.get()) {
        case 'h': case 'H': help();    break;
        case 'd': case 'D': display(); break;
        case 'q': case 'Q': quit();    break;
        case '\n':                     break;
        default:            help();    break;
    }
}

void stdio_transport::run_forever()
{
    _running = true;
    while (_running) {
        run_once();
    }

    std::cout << "Goodbye" << std::endl;
}

void stdio_transport::help()
{
    std::cout << "Commands:\n"
              << "\th - Show this help\n"
              << "\td - Display sensor data\n"
              << "\tq - Quit\n"
              << std::endl;
}

void stdio_transport::display()
{
    static constexpr unsigned MAX_ENTRY_VALUES = 3;
    static constexpr unsigned MAX_PACKET_ENTRIES = static_cast<unsigned>(device_type::_max) - static_cast<unsigned>(drivers::device_type::_min) - 1;
    static constexpr unsigned PACKET_ENTRY_SIZE = packet::entry::size(MAX_ENTRY_VALUES);
    static constexpr unsigned PACKET_SIZE = packet::size(PACKET_ENTRY_SIZE, MAX_PACKET_ENTRIES);

    std::array<uint8_t, PACKET_SIZE> bytes;
    auto pkt = reinterpret_cast<packet*>(&bytes[0]);
    {
        std::memset(pkt, 0, PACKET_SIZE);
        device_type type = device_type::_min;
        unsigned i;
        for (i = 0u; i < MAX_PACKET_ENTRIES; i++) {
            pkt->entries[i].type = type;
            pkt->entries[i].count = MAX_ENTRY_VALUES;
            type = static_cast<device_type>(static_cast<unsigned>(type) + 1);
        }
        pkt->count = i;

        get_event_handler()->on_sensor_update(pkt);
    }

    for (auto i = 0u; i < pkt->count; i++) {
        auto& entry = pkt->entries[i];
        std::cout << device_type_str(entry.type) << ": ";
        for (auto j = 0u; j < entry.count; j++) {
            std::cout << entry.values[j] << ' ';
        }

        std::cout.put('\n');
    }
}

void stdio_transport::quit()
{
    _running = false;
}
}} // fp::transports
