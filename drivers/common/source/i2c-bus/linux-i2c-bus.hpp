/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <cstddef>
#include <cstdint>
#include <map>
#include <mutex>
#include <string>

#include <fp/drivers/i2c-bus.hpp>

namespace fp { namespace drivers {
struct linux_i2c_bus : i2c_bus, public std::enable_shared_from_this<linux_i2c_bus>
{
    static bool is_detected(int bus_id);

    linux_i2c_bus(int bus_id);

    ~linux_i2c_bus();

    int acquire();

    void release();

    std::shared_ptr<i2c_device> get_device(uint16_t address) override;
private:
    std::mutex _lock;
    std::string _node_path;
    int _handle;

    static std::string get_node_path(int bus_id);
};

struct linux_i2c_device : i2c_device {
    linux_i2c_device(std::shared_ptr<linux_i2c_bus> bus, uint16_t address);

    int read(uint8_t reg);

    int read(uint8_t reg, uint8_t* buffer, size_t& count);

    int write(uint8_t reg, uint8_t byte);

    int write(uint8_t reg, const uint8_t* buffer, size_t& count);
private:
    std::shared_ptr<linux_i2c_bus> _bus;
    uint16_t _address;

    int acquire_bus();

    int internal_read(int handle, uint8_t* buffer, size_t& count);

    int internal_write(int handle, const uint8_t* buffer, size_t& count);
};
}} // fp::drivers
