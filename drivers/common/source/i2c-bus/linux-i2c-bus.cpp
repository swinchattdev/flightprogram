/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <algorithm>
#include <cstdint>
#include <cstdlib>
#include <cstring>
#include <errno.h>
#include <exception>
#include <iomanip>
#include <iostream>
#include <memory>
#include <map>
#include <mutex>
#include <sstream>
#include <stdexcept>

#include <dirent.h>
#include <fcntl.h>
#include <linux/i2c.h>
#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <unistd.h>

#include <fp/drivers/i2c-bus.hpp>
#include <fp/util/string-util.hpp>

#include "linux-i2c-bus.hpp"

#ifndef I2C_DEV_PATH
# define I2C_DEV_PATH "/sys/class/i2c-dev/"
#endif

using namespace fp::util;

namespace fp { namespace drivers {
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// linux_i2c_bus
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
std::string linux_i2c_bus::get_node_path(int bus_id)
{
    std::ostringstream ss;
    ss << "/dev/i2c-" << bus_id;
    return ss.str();
}

bool linux_i2c_bus::is_detected(int bus_id)
{
    std::string path(get_node_path(bus_id));
    return access(path.c_str(), F_OK) == 0;
}

linux_i2c_bus::linux_i2c_bus(int bus_id) : _node_path(get_node_path(bus_id)) {}

linux_i2c_bus::~linux_i2c_bus()
{
    close(_handle);
}

int linux_i2c_bus::acquire()
{
    if (_handle == 0)
    {
        _handle = open(_node_path.c_str(), O_RDWR);
        if (_handle < 0)
        {
            auto s = format_strerror_message("open " + _node_path);
            return -1;
        }
    }

    _lock.lock();
    return _handle;
}

void linux_i2c_bus::release()
{
    _lock.unlock();
}

std::shared_ptr<i2c_device> linux_i2c_bus::get_device(uint16_t address)
{
    auto p = std::make_shared<linux_i2c_device>(shared_from_this(), address);
    return std::static_pointer_cast<i2c_device>(p);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// linux_i2c_device
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
linux_i2c_device::linux_i2c_device(std::shared_ptr<linux_i2c_bus> bus, uint16_t address)
: _bus(bus)
, _address(address)
{}

int linux_i2c_device::read(uint8_t reg)
{
    auto handle = acquire_bus();
    if (handle < 0) {
        return -1;
    }

    uint8_t buffer[1] = {reg};
    auto size = sizeof(buffer);
    auto result = internal_read(handle, buffer, size);
    if (result == 0) {
        result = buffer[0];
    }

    _bus->release();
    return result;
}

int linux_i2c_device::read(uint8_t reg, uint8_t* buffer, size_t& count)
{
    auto handle = acquire_bus();
    if (handle < 0) {
        return -1;
    }

    buffer[0] = reg;
    auto result = internal_read(handle, buffer, count);
    _bus->release();
    return result;
}

int linux_i2c_device::write(uint8_t reg, uint8_t byte)
{
    auto handle = acquire_bus();
    if (handle < 0) {
        return -1;
    }

    uint8_t buffer[2] = {reg, byte};
    auto size = sizeof(buffer);
    auto result = internal_write(handle, buffer, size);
    _bus->release();
    return result;
}

int linux_i2c_device::write(uint8_t reg, const uint8_t* buffer, size_t& count)
{
    auto handle = acquire_bus();
    if (handle < 0) {
        return -1;
    }

    auto wrbuf = new uint8_t[count + 1];
    wrbuf[0] = reg;
    std::copy(buffer, buffer + count, wrbuf + 1);
    auto result = internal_write(handle, wrbuf, count);
    delete[] wrbuf;
    _bus->release();
    return result;
}

int linux_i2c_device::acquire_bus()
{
    auto handle = _bus->acquire();
    if (handle < 0) {
        return -1;
    }

    if (ioctl(handle, I2C_SLAVE_FORCE, _address) < 0) {
        std::clog << format_strerror_message("ioctl") << std::endl;
    }

    return handle;
}

int linux_i2c_device::internal_read(int handle, uint8_t* buffer, size_t& count)
{
    auto result = ::read(handle, buffer, count);
    if (result < 0) {
        std::clog << format_strerror_message("read") << std::endl;
    }

    return result;
}

int linux_i2c_device::internal_write(int handle, const uint8_t* buffer, size_t& count)
{
    auto result = ::write(handle, buffer, count);
    if (result < 0) {
        std::clog << format_strerror_message("write") << std::endl;
    }

    return result;
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// i2c_bus
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int i2c_bus::get_max_bus_id()
{
    static int max_bus_id = -1;
    if (max_bus_id >= 0) {
        return max_bus_id;
    }

    DIR* dir = opendir(I2C_DEV_PATH);
    if (!dir) {
        std::clog << format_strerror_message("opendir " I2C_DEV_PATH) << std::endl;
        return -1;
    }

    while (true) {
        auto entry = readdir(dir);
        if (entry == nullptr) {
            break;
        }

        if (std::strcmp(entry->d_name, "i2c-") == 0) {
            int bus_id = atoi(&entry->d_name[4]);
            if (bus_id > max_bus_id) {
                max_bus_id = bus_id;
            }
        }
    }

    closedir(dir);
    return max_bus_id;
}

std::shared_ptr<i2c_bus> i2c_bus::get_instance(int bus_id)
{
    static std::map<int, std::shared_ptr<linux_i2c_bus>> instances;
    auto it = instances.find(bus_id);
    if (it == instances.end()) {
        if (linux_i2c_bus::is_detected(bus_id)) {
            instances[bus_id].reset(new linux_i2c_bus(bus_id));
        }

        return instances[bus_id];
    }

    return it->second;
}
}} // fp::drivers