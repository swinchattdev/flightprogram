# FlightProgram

Flight control program & drivers for Linux.

## Drivers

[Read Me](drivers/ReadMe.md)

Standalone userspace binaries that each drive a sensor or actuator e.g. using `/dev/i2c-*`.

## Control program

[Read Me](controller/ReadMe.md)

Spawns drivers as subprocesses and communicates with them via different transports.

## Common library

[Read Me](common/ReadMe.md)

Shared library for common code.
