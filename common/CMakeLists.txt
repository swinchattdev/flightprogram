add_library(fpcommon STATIC)

target_include_directories(
    fpcommon
        PUBLIC
            ./include/
)

target_sources(
    fpcommon
        PUBLIC
            ./source/util/cmdline.cpp
            ./source/util/fork-exec.cpp
            ./source/util/signals.cpp
            ./source/util/string-util.cpp
)
