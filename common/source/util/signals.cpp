/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <functional>
#include <map>
#include <vector>

#include <signal.h>

namespace fp { namespace util {
    static std::map<int, std::vector<std::function<void()>>*> signal_handlers;

    static void generic_signal_handler(int signum)
    {
        auto it = signal_handlers.find(signum);
        if (it == signal_handlers.end()) {
            return;
        }

        for (auto fn : *(it->second)) {
            fn();
        }
    }

    static int register_generic_signal_handler(int signum)
    {
        struct sigaction sa;
        sa.sa_handler = generic_signal_handler;
        sigemptyset(&sa.sa_mask);
        sa.sa_flags = 0;
        return sigaction(signum, &sa, nullptr);
    }

    static int find_or_create_handlers_for_signum(
        int signum,
        std::vector<std::function<void()>>*& new_list,
        std::vector<std::function<void()>>*& old_list)
    {
        auto it = signal_handlers.find(signum);
        if (it == signal_handlers.end()) {
            if (register_generic_signal_handler(signum) < 0) {
                return -1;
            }

            new_list = new std::vector<std::function<void()>>();
        } else {
            new_list = new std::vector<std::function<void()>>(*(it->second));
            old_list = it->second;
        }

        return 0;
    }

    int register_signal_handler(int signum, std::function<void()> handler)
    {
        if (signum == 0) {
            return 0;
        }

        std::vector<std::function<void()>>* new_list = nullptr;
        std::vector<std::function<void()>>* old_list = nullptr;
        if (find_or_create_handlers_for_signum(signum, new_list, old_list) < 0) {
            return -1;
        }

        new_list->push_back(handler);
        signal_handlers[signum] = new_list;
        delete old_list;
        return 0;
    }
}}
