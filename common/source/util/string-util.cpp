/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <iostream>
#include <string>
#include <vector>

namespace fp { namespace util {
void tokenise(const std::string& string, const std::string& separator, std::vector<std::string>& tokens)
{
    size_t start = 0;
    size_t end = 0;
    while (end != std::string::npos) {
        end = string.find(separator, start);
        tokens.push_back(string.substr(start, end));
        start = end + separator.size();
    }
}

std::string format_strerror_message(const std::string& msg)
{
    return msg + ": " + std::strerror(errno);
}
}} // fp::util
