/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <algorithm>
#include <exception>
#include <functional>
#include <sstream>
#include <stdexcept>
#include <string>
#include <vector>

#include <sys/wait.h>
#include <unistd.h>

#include <fp/util/fork-exec.hpp>

namespace fp { namespace util {
static void dup2_or_die(int a, int b)
{
    if (dup2(a, b) < 0) {
        perror("dup2");
        exit(1);
    }
}

int fork_exec(
    const std::string& path,
    const std::vector<std::string>& args,
    int(& fd)[2],
    redirection_t redir
)
{
    auto using_pipe = false;
    if (fd && redir) {
        if (pipe(fd) < 0) {
            perror("pipe");
            return -1;
        }

        using_pipe = true;
        if ((redir & redirection::stdin) && (((redir & redirection::stdout) || (redir & redirection::stderr)))) {
            throw std::logic_error("Tried to redirect stdin and stdout/stderr to the same pipe");
        }
    }

    auto pid = fork();
    if (pid < 0) {
        perror("fork");
    } else if (pid) {
        if (using_pipe) {
            if (redir & redirection::stdin) {
                close(fd[1]);
                dup2_or_die(fd[0], 0);
            } else {
                close(fd[0]);
                if (redir & redirection::stdout || redir & redirection::stderr) {
                    if (redir & redirection::stdout) {
                        dup2_or_die(fd[1], 1);
                    }
                    if (redir & redirection::stderr) {
                        dup2_or_die(fd[1], 2);
                    }
                }
            }
        }

        {
            std::vector<char*> argv(argv.size());
            std::transform(
                std::begin(args),
                std::end(args),
                std::back_inserter(argv),
                [](const std::string& s) { return const_cast<char*>(s.c_str()); }
            );

            execv(path.c_str(), &argv[0]);
        }

        {
            std::ostringstream ss;
            ss << "execv \"" << path << "\"";
            perror(ss.str().c_str());
            exit(1);
        }
    }

    return pid;
}
}} // fp::util