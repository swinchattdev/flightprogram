/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#include <algorithm>
#include <iostream>
#include <map>
#include <string>
#include <vector>

#include <fp/util/cmdline.hpp>

namespace fp { namespace util {
cmdline_option::cmdline_option(const std::string& name_, const std::string& help_)
: name(name_)
, help(help_)
{}

cmdline_option::cmdline_option(const std::string& name_, const std::string& arg_name_, bool arg_optional_, const std::string& help_)
: name(name_)
, arg_name(arg_name_)
, arg_optional(arg_optional_)
, help(help_)
{}

cmdline_option::cmdline_option(const std::string& section_, const std::string& name_, const std::string& help_)
: section(section_)
, name(name_)
, help(help_)
{}

cmdline_option::cmdline_option(
    const std::string& section_,
    const std::string& name_,
    const std::string& arg_name_,
    bool arg_optional_,
    const std::string& help_
)
: section(section_)
, name(name_)
, arg_name(arg_name_)
, arg_optional(arg_optional_)
, help(help_)
{}

cmdline_result::cmdline_result() : _ok(true)
{}

cmdline_result::cmdline_result(const std::string& error_) : _error(error_)
{}

bool cmdline_result::ok() const
{
    return _ok;
}

const std::string& cmdline_result::error() const
{
    return _error;
}

void print_options(
    const std::vector<cmdline_option>& options,
    std::ostream& stream,
    const std::string& indent,
    int initial_level)
{
    std::map<std::string, std::vector<cmdline_option>> sections;

    for (auto option : options) {
        sections.insert(std::make_pair(option.section, std::vector<cmdline_option>()));
    }

    for (auto option : options) {
        sections[option.section].push_back(option);
    }

    for (auto section : sections) {
        // Print section name indented to initial_level.
        stream.put('\n');
        for (auto i = 0; i < initial_level; i++) {
            stream << indent;
        }

        if (section.first.size()) {
            stream << section.first << ":\n";
        } else {
            stream << "Top-level options:\n";
        }

        // Print options indented one additional level.
        if (section.second.size() == 1 && section.second[0].name == "") {
            for (auto i = 0; i < initial_level + 1; i++) {
                stream << indent;
            }

            stream << "(No options in section)\n";
            continue;
        }

        for (auto option : section.second) {
            for (auto i = 0; i < initial_level + 1; i++) {
                stream << indent;
            }

            stream << "-" << option.name;
            if (option.arg_name.size()) {
                char rhs;
                if (option.arg_optional) {
                    stream.put('[');
                    rhs = ']';
                } else {
                    stream.put('<');
                    rhs = '>';
                }

                stream << ' ' << option.arg_name;
                stream.put(rhs);
            } else {
                stream << "\t";
            }

            stream << '\t' << option.help << '\n';
        }
    }
}

cmdline_result parse_args(
    int argc,
    char** argv,
    const std::vector<cmdline_option>& options,
    std::map<std::string, std::string>& vm,
    std::vector<std::string>& positionals
)
{
    cmdline_option* last_opt = nullptr;
    for (auto i = 1; i < argc; i++) {
        std::string arg(argv[i]);
        if (last_opt) {
            if (last_opt->arg_optional && arg.size() && arg[0] == '-') {
                vm[last_opt->name] = "";
            } else {
                if (arg.size() && arg[0] == '\\') {
                    arg = arg.substr(1);
                }

                vm[last_opt->name] = arg;
            }

            last_opt = nullptr;
            continue;
        }

        if (!arg.size()) {
            continue;
        }

        if (arg[0] != '-') {
            positionals.push_back(arg);
            continue;
        }

        arg = arg.substr(1);

        bool handled = false;
        for (auto option : options) {
            if (option.name == arg) {
                if (option.arg_name.size()) {
                    last_opt = reinterpret_cast<cmdline_option*>(&arg);
                } else {
                    vm[option.name] = "";
                }

                handled = true;
                break;
            }
        }

        if (!handled) {
            return cmdline_result(std::string("Error: Unexpected option \"") + arg + "\" on command-line");
        }
    }

    return cmdline_result();
}
}} // fp::util
