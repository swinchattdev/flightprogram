/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <exception>
#include <stdexcept>

#define __STRINGIFY(X) #X
#define STRINGIFY(X) __STRINGIFY(X)

#define __LINE_STR__ STRINGIFY(__LINE__)
#define __FILE_LINE__ __FILE__ ":" __LINE_STR__

#define THROW_UNREACHABLE() throw std::logic_error(__FILE_LINE__ ": Unreachable code")

using real_t = float;
