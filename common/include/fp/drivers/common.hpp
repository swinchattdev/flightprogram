/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <cstdint>

#include <fp/common.hpp>

namespace fp { namespace drivers {
/// Device type.
enum class device_type {
    _min = 0,
    accelerometer = 0,
    barometer,
    gyroscope,
    magnetometer,
    servo_esc,
    thermometer,
    _max,
};

inline const char* device_type_str(device_type type)
{
    switch (type) {
        case device_type::accelerometer: return "accelerometer";
        case device_type::barometer:     return "barometer";
        case device_type::gyroscope:     return "gyroscope";
        case device_type::magnetometer:  return "magnetometer";
        case device_type::servo_esc:     return "servo_esc";
        case device_type::thermometer:   return "thermometer";
        default:                         return "invalid";
    }
}

/// Device info.
struct device_info {
    /// The device type.
    device_type type;

    /// The device count.
    int count;

    device_info() {}

    device_info(device_type type_, int count_)
    : type(type_)
    , count(count_)
    {}
};

/// Data packet of arbitrary size.
struct packet {
    /// Packet entry of arbitrary size.
    struct entry {
        /// The device type, \see device_type.
        device_type type;

        /// The number of values.
        uint16_t count;

        /// One or more values.
        real_t values[];

        static constexpr size_t size(size_t num_values)
        {
            return sizeof(entry::type)
                 + sizeof(entry::count)
                 + sizeof(entry::values[0])*num_values
            ;
        }
    };

    /// The number of entries.
    uint8_t count;

    /// One or more entries.
    entry entries[];

    static constexpr size_t size(size_t entry_size, size_t num_entries)
    {
        return sizeof(count) + entry_size*num_entries;
    }
};
}} // fp::drivers
