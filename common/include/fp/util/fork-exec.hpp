/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <functional>
#include <string>
#include <vector>

namespace fp { namespace util {
/// Stdio redirection.
struct redirection {
    enum t {
        none   = 0,
        stdin  = 1 << 0,
        stdout = 1 << 1,
        stderr = 1 << 2
    };
};

using redirection_t = redirection::t;

// Creates a child process using fork() and executes the executable argv[0] with arguments argv[1:], with optional
/// redirection of standard streams to/from a pipe.
/// \param[in] path The path to pass to execv().
/// \param[in] argv Zero or more command-line arguments.
/// \param[out] fd A pointer to an array of at least two elements, into which the read and write file descriptors shall
/// be stored.
/// \param[in] redir Whether to redirect the child's standard input, output and error to/from the pipe.
/// \returns The child's process ID on success, or -1 if pipe() or fork() returns an error code.
/// \note The value of 'redir' is ignored when 'fd' is nullptr and vice versa.
int fork_exec(
    const std::string& path,
    const std::vector<std::string>& args,
    int(& fd)[2],
    redirection_t redir
);
}} // fp::util
