/*
 * Copyright (C) 2021 Chris Swinchatt
 *
 * FlightProgram is free software: you can redistribute it and/or modify it
 * under the terms of the GNU Affero General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or (at your
 * option) any later version.
 *
 * FlightProgram is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with FlightProgram.  If not, see <http://www.gnu.org/licenses/>.
 */
#pragma once

#include <iosfwd>
#include <map>
#include <string>
#include <vector>

namespace fp { namespace util {
/// Describes a command-line option.
struct cmdline_option {
    /// Optional section.
    std::string section;

    /// The option name (no leading -).
    std::string name;

    /// Argument name, if the option takes an argument.
    std::string arg_name;

    /// Whether the argument is optional.
    bool arg_optional;

    /// The help message.
    std::string help;

    cmdline_option(const std::string& name_, const std::string& help_);

    cmdline_option(
        const std::string& name_,
        const std::string& arg_name_,
        bool arg_optional_,
        const std::string& help_
    );

    cmdline_option(const std::string& section_, const std::string& name_, const std::string& help_);

    cmdline_option(
        const std::string& section_,
        const std::string& name_,
        const std::string& arg_name_,
        bool arg_optional_,
        const std::string& help_
    );
};

/// Interface implemented by objects which take options from the command-line.
struct has_cmdline_options {
    /// Adds accepted command-line options to 'options'.
    virtual void add_options(std::vector<cmdline_option>& options) = 0;

    /// Initialises with the parsed command-line options.
    virtual int init(const std::map<std::string, std::string>& vm) = 0;
protected:
    ~has_cmdline_options() {}
};

/// Result of command-line parsing.
struct cmdline_result {
    cmdline_result();

    cmdline_result(const std::string& error_);

    bool ok() const;

    const std::string& error() const;
private:
    bool _ok;
    std::string _error;
};

/// Print a friendly description of 'options' to 'stream'.
/// \param[in] options The options.
/// \param[out] stream The output stream.
/// \param[in] indent The indent character.
/// \param[in] initial_level The number of intentations for section names. Options will be indented an additional
/// level.
void print_options(
    const std::vector<cmdline_option>& options,
    std::ostream& stream,
    const std::string& indent = "\t",
    int initial_level = 1
);

/// Parse the command-line arguments 'argv' based on 'options' and store the options in 'vm' and positional arguments in
/// 'positional_args'.
cmdline_result parse_args(
    int argc,
    char** argv,
    const std::vector<cmdline_option>& options,
    std::map<std::string, std::string>& vm,
    std::vector<std::string>& positional_args
);
}} // fp::util
